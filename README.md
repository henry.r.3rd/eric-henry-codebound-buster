Today we will be working on updating Codebound Buster website!

INSTRUCTIONS

Fork this repository
Clone the forked repository.
Read through the existing code that is already on here.


TODO
For this project, we will be using axios from NPM.

Search ‘NPM axios’
Include axios in your html file via cdn.
Visit http://www.omdbapi.com/

Generate an API Key
- Account Type select FREE!
- Enter your email address
Your key should be sent to your email address (READ through the email)

Tip: You will have to make a request to the OMDb API using axios.
READ through the axios webpage on how to perform a request
Stories:

As a user, I would like the name of the website to be centered above the search form.
As a user, I would like a different color scheme and over theme to the webpage.
As a user, I would like the placeholder of the form to say something different.
As a user, I would like a navbar with a logo, and the name of the webpage.
As a user, I would like a link in the navbar that will send me to a webpage to Blockbuster's History.
As a user, I would like to be able to type any title of a movie, hit enter, and see the movie's poster, title, year of release, and a button that's labeled 'imdb'.
As a user, I would like an organized row(s) whenever the data is rendered.
As a user, I would like to be taken to the movie's imdb webpage/profile once I click on the 'imdb' button.

Feature:

As a user, when I click on the movie' poster, I would see more details about that movie.

High-Feature:

Create your own db.json. This will be an array of objects with minimum four movies with properties of: "id", "title", "year", "genre"
Create a form where you would POST to this db.json
Create a REQUEST where you can DELETE from the db.json